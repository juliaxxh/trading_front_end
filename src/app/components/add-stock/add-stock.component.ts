import { Component, OnInit } from '@angular/core';
import { CommonModule } from "@angular/common";
import { StocksService } from 'src/app/services/stocks.service';
@Component({
  selector: 'app-add-stock',
  templateUrl: './add-stock.component.html',
  styleUrls: ['./add-stock.component.css']
})
export class AddStockComponent implements OnInit {

  stock = {
    ticker: '',
    stockQuantity: 0,
    requestedPrice:0,
    type:''
  };
  submitted = false;

  constructor(private stocksService: StocksService) { }

  ngOnInit(): void {
  }

  saveStock(type): void {
    const data = {
      ticker: this.stock.ticker,
      stockQuantity: this.stock.stockQuantity,
      requestedPrice: this.stock.requestedPrice,
      type:this.stock.type
    };

    this.stocksService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newStock(): void {
    this.submitted = false;
    this.stock = {
      ticker: '',
      stockQuantity: 0,
      requestedPrice: 0,
      type: ''  };
  }

  buy():void{
    this.stock.type = "BUY";
  }

  sell():void{
    this.stock.type = "SELL";
  }
}
