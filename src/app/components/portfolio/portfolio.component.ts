import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent {
  url = `http://localhost:8080/portfolio`;
  // Chart
  chartLabels = [];
  chartData = Array(1000).fill(0);     // HACKY WAY TO GET COLOUR IN THE CHART WHEN chartData is loaded dynamically! TO INVESTIGATE LATER!
  chartType = 'doughnut' as const;
  private i = 0;

  constructor(private http: HttpClient) {
    this.http.get(this.url).toPromise().then(data => {
      for (const [key, value] of Object.entries(data)) {
        if (value > 0) {
          this.chartLabels[this.i] = key;
          this.chartData[this.i] = value;
          this.i++;
        }
      }
      this.chartData.splice(this.i + 1);   // Remove the extra ~1000 elements!
    });
  }

  // Events
  public chartClicked(e: any): void {}

  public chartHovered(e: any): void {}
}
