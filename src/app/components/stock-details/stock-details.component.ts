import { Component, OnInit } from '@angular/core';
import { StocksService } from 'src/app/services/stocks.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stock-details',
  templateUrl: './stock-details.component.html',
  styleUrls: ['./stock-details.component.css']
})
export class StockDetailsComponent implements OnInit {

  currentStock = null;
  message = '';

  constructor(
    private stocksService: StocksService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getStock(this.route.snapshot.paramMap.get('id'));
  }

  getStock(id): void {
    this.stocksService.get(id)
      .subscribe(
        data => {
          this.currentStock = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updateState(requestedPrice): void {
    const data = {
      ticker: this.currentStock.ticker,
      stockQuantity: this.currentStock.stockQuantity,
      requestedPrice: requestedPrice
    };

    this.stocksService.update(this.currentStock.id, data)
      .subscribe(
        response => {
          this.currentStock.requestedPrice = requestedPrice;
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  updateStock(): void {
    this.stocksService.update(this.currentStock.id, this.currentStock)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The stock was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteStock(): void {
    this.stocksService.delete(this.currentStock.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/stocks']);
        },
        error => {
          console.log(error);
        });
  }
}
