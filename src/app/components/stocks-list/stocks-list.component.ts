import { Component, OnInit } from '@angular/core';
import { StocksService } from 'src/app/services/stocks.service';
import { CommonModule } from "@angular/common";
@Component({
  selector: 'app-stocks-list',
  templateUrl: './stocks-list.component.html',
  styleUrls: ['./stocks-list.component.css']

})

export class StocksListComponent implements OnInit {

  stocks: any;
  currentStock = null;
  currentIndex = -1;
  ticker = '';

  constructor(private stocksService: StocksService) { }

  ngOnInit(): void {
    this.stocksService.getAll()
      .subscribe(
        data => {
          this.stocks = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  retrieveStocks(): void {
    this.stocksService.getAll()
      .subscribe(
        data => {
          this.stocks = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveStocks();
    this.currentStock = null;
    this.currentIndex = -1;
  }

  setActiveStock(stock, index): void {
    this.currentStock = stock;
    this.currentIndex = index;
  }

  removeAllStocks(): void {
    this.stocksService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveStocks();
        },
        error => {
          console.log(error);
        });
  }

  searchTicker(): void {
    this.stocksService.findByTicker(this.ticker)
      .subscribe(
        data => {
          this.stocks = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
}
