import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StocksListComponent } from './components/stocks-list/stocks-list.component';
import { StockDetailsComponent } from './components/stock-details/stock-details.component';
import { AddStockComponent } from './components/add-stock/add-stock.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
const routes: Routes = [
  { path: '', redirectTo: 'stocks', pathMatch: 'full' },
  { path: 'stocks', component: StocksListComponent },
  { path: 'stocks/:id', component: StockDetailsComponent },
  { path: 'add', component: AddStockComponent },
  { path: 'portfolio', component: PortfolioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
