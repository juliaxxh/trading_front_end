import io
import random
from flask import Response
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pandas_datareader as pdr
from matplotlib.legend import Legend
from yahoo_fin import stock_info as si
import pandas as pd
from flask import Flask
import datetime as dt
import os
from flask import Flask, Response, render_template
import requests
from yahooquery import Ticker
import plotly.graph_objects as go
from yahoo_fin import options
import json
import plotly

demo = "143099c89da2e9e2078cd87576700315"

app = Flask(__name__)
today = dt.datetime.today().strftime('%Y-%m-%d')
@app.route('/plot/<ticker>')
def plot_png(ticker):
    fig = create_figure(ticker)
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='new_plot.png')

@app.route('/plot/volatility/<ticker>')
def plot_png2(ticker):
    fig = create_figure2(ticker)
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='new_plot2.png')

def get_data(ticker, start_date, end_date):
        return pdr.get_data_yahoo(ticker, start_date, end_date)
def create_figure(ticker):
    # fig = Figure()
    # axis = fig.add_subplot(1, 1, 1)
    # xs = range(100)
    # ys = [random.randint(1, 50) for x in xs]
    # axis.plot(xs, ys)
    # return fig

    start = '2019'
    end = '2020'
    df_ticker = get_data(ticker,'2019-09-04', today)
    sma1 = df_ticker['Adj Close'].rolling(window=21).mean()
    sma = df_ticker['Adj Close'].rolling(window=21).mean()
    symbol = 'AAPL'
# calculate the standar deviation
    rstd = df_ticker['Adj Close'].rolling(window=21).std()

    upper_band = sma + 2 * rstd
# upper_band = upper_band.rename(columns={symbol: 'upper'})

    lower_band = sma - 2 * rstd
# lower_band = lower_band.rename(columns={symbol: 'lower'})
    m_avg = sma

    df_ticker['Upper'] = upper_band #throws an error here
    df_ticker['Lower'] = lower_band
    df_ticker['M avg'] = sma1
    plt.style.use('fivethirtyeight')
    fig = plt.figure(figsize=(12,6))
    ax = fig.subplots(1)
    x_axis = df_ticker['2019':'2020'].index.get_level_values(0)
    ax.fill_between(x_axis,
                df_ticker[start:end]['Upper'],
                df_ticker[start:end]['Lower'],
                color='grey')
    ax.set_ylabel(ticker+" Price",color="red",fontsize=14)
# ax.yscale('log')
# ax.plot(x_axis, df_vix[start:end][col], color='blue', lw=2)
    ax.plot(x_axis, df_ticker[start:end]['M avg'], label=ticker,color='green', lw=2)
# ax.plot(x_axis1, df_tsla[start:end]['M avg'], color='red', lw=2)
    plt.savefig('new_plot.png')
    return fig

@app.route('/<tickerSymbol>')
def index(tickerSymbol):
    return render_template('index.html', tickerSymbol=tickerSymbol)

@app.route('/data/<tickerSymbol>')
def view(tickerSymbol):
    def stream():
        while True:
            ticker = Ticker(tickerSymbol)
            quotes = ticker.quotes[0]
            try:
                price = quotes["preMarketPrice"]
                points = quotes["preMarketChange"]
                percent = quotes["preMarketChangePercent"]
            except KeyError:
                try:
                    price = quotes["postMarketPrice"]
                    points = quotes["postMarketChange"]
                    percent = quotes["postMarketChangePercent"]
                except KeyError:
                    price = quotes["regularMarketPrice"]
                    points = quotes["regularMarketChange"]
                    percent = quotes["regularMarketChangePercent"]
            plus_sign = "+" if points >= 0 else ""
            advice = ticker.financial_data[tickerSymbol]["recommendationKey"].capitalize()
            output = f"{tickerSymbol.upper()} {price:.2f} {plus_sign}{points:.2f} {'('}{plus_sign}{percent:.2f}{'%)'} {advice}"
            yield f"data:{output}\n\n"
    return Response(stream(), mimetype='text/event-stream')

def create_figure2(ticker):
    start = '2019'
    end = '2020'
    df_ticker = get_data(ticker,'2019-09-04', '2020-09-04')
    df_vix = get_data('^VIX','2019-09-04', '2020-09-04')
    sma1 = df_ticker['Volume'].rolling(window=21).mean()
    sma2 = df_vix['Adj Close'].rolling(window=21).mean()
    print(len(sma1) == len(sma2))
    df_ticker['M avg'] = sma1
    df_vix['M avg'] = sma2
    plt.style.use('fivethirtyeight')
    fig = plt.figure(figsize=(12,6))
    ax = fig.subplots(1)
    x_axis1 = df_ticker['2019':'2020'].index.get_level_values(0)
    x_axis2 = df_vix['2019':'2020'].index.get_level_values(0)
    ax.set_ylabel(ticker+" Volume",color="red",fontsize=14)
# ax.yscale('log')
    ax2=ax.twinx()
    ax2.set_ylabel("VIX close",color="red",fontsize=14)
# ax.plot(x_axis, df_vix[start:end][col], color='blue', lw=2)
    ax.plot(x_axis1, df_ticker[start:end]['M avg'], label=ticker,color='green', lw=2)
# ax.plot(x_axis1, df_tsla[start:end]['M avg'], color='red', lw=2)
    ax2.plot(x_axis2, df_vix[start:end]['M avg'], label='VIX',color='blue', lw=2)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    return fig

def get_table(company):
    FinMetrics = {}
    companydata = requests.get(f'https://financialmodelingprep.com/api/v3/profile/{company}?apikey={demo}').json()
    IS = requests.get(f'https://financialmodelingprep.com/api/v3/income-statement/{company}?period=quarter&apikey={demo}').json()
    BS = requests.get(f'https://financialmodelingprep.com/api/v3/balance-sheet-statement/{company}?period=quarter&apikey={demo}').json()
    CF = requests.get(f'https://financialmodelingprep.com/api/v3/cash-flow-statement/{company}?period=quarter&apikey={demo}').json()
    #revenue last 4 years

    if  len(IS) < 39:
        revenue = [IS[0]['revenue'],IS[1]['revenue'],IS[2]['revenue'],IS[3]['revenue']]
        revenue = np.array(revenue).sum()
        net_income = [IS[0]['netIncome'],IS[1]['netIncome'],IS[2]['netIncome'],IS[3]['netIncome']]
        net_income = np.array(net_income).sum()

        FCF = CF[0]['freeCashFlow'] + CF[1]['freeCashFlow'] + CF[2]['freeCashFlow'] + CF[3]['freeCashFlow']
        OperatingCF = CF[0]['operatingCashFlow'] + CF[1]['operatingCashFlow'] + CF[2]['operatingCashFlow'] + CF[3]['operatingCashFlow']
        total_debt = BS[0]['totalDebt']

        eps_diluted = [IS[0]['epsdiluted'],IS[1]['epsdiluted'],IS[2]['epsdiluted'],IS[3]['epsdiluted']]
        eps_diluted = np.array(eps_diluted).sum()

        total_shareholders_equity = BS[0]['totalStockholdersEquity']
        shareholders_equity_2_quarters_Average = (BS[0]['totalStockholdersEquity'] + BS[1]['totalStockholdersEquity']) / 2

        total_assets_2qts = (BS[0]['totalAssets'] + BS[1]['totalAssets']) / 2

        latest_Annual_Dividend = companydata[0]['lastDiv']
        price = companydata[0]['price']
        market_Capitalization = companydata[0]['mktCap']
        name = companydata[0]['companyName']
    # exchange = companydata[0]['exchange']


        grossprofitma12TTMgrowht = ((IS[0]['grossProfitRatio'] - IS[4]['grossProfitRatio']) / IS[4]['grossProfitRatio']) *100 # add to fin metrics. Seems flawed
        if latest_Annual_Dividend is None:
            latest_Annual_Dividend = 0
        dividend_Yield= latest_Annual_Dividend/price
        FinMetrics[company] = {}
    # FinMetrics[company]['Dividend_Yield'] = dividend_Yield * 100
        FinMetrics[company]['latest price'] = price
    # FinMetrics[company]['latest_Dividend'] = latest_Annual_Dividend
    # FinMetrics[company]['market_Capit_in_M'] = market_Capitalization/1000000
        FinMetrics[company]['P/E (diluted)'] = price / eps_diluted
        FinMetrics[company]['P/S'] = market_Capitalization / revenue
        FinMetrics[company]['P/BV'] = market_Capitalization / (total_shareholders_equity - BS[0]['goodwillAndIntangibleAssets'])
    # FinMetrics[company]['PEG'] = FinMetrics[company]['pe'] / EBITDA5YGrowht
        FinMetrics[company]['Gross profit ratio'] = IS[0]['grossProfitRatio']
        FinMetrics[company]['latest Financials date'] = IS[0]['date']
        FinMetrics[company]['Gross profit ratio growth 1 yr'] = grossprofitma12TTMgrowht
    # FinMetrics[company]['Revenue of last6qts in millions'] = [IS[0]['revenue'],IS[1]['revenue'],IS[2]['revenue'],IS[3]['revenue'],IS[4]['revenue'],IS[5]['revenue']]
    # FinMetrics[company]['Revenue of last6qts in millions'] = np.array(FinMetrics[company]['Revenue_last6qts_inM']) / 1000000

        FinMetrics[company]['P/CFO'] = market_Capitalization / OperatingCF

        FinMetrics[company]['P/FCF'] = market_Capitalization / FCF
        FinMetrics[company]["Current Ratio"] = BS[0]['totalCurrentAssets']/BS[0]['totalCurrentLiabilities']
        FinMetrics[company]["Quick Ratio"] = BS[0]['cashAndShortTermInvestments']/BS[0]['totalCurrentLiabilities']
        FinMetrics[company]['Debt to Equity'] = total_debt / total_shareholders_equity
        FinMetrics[company]['ROE'] = net_income / shareholders_equity_2_quarters_Average
        FinMetrics[company]['ROA'] = net_income / total_assets_2qts
        FinMetrics[company]['Interest Coverage'] = (IS[0]['ebitda'] - IS[0]['depreciationAndAmortization'])/IS[0]['interestExpense']

        # FinMetrics[company]['revenue growth from last year'] = ((IS[0]['revenue'] -  IS[3]['revenue'])/ IS[3]['revenue']) *100
        # FinMetrics[company]['earnings growth from last year'] = ((IS[0]['netIncome'] -  IS[3]['netIncome'])/ IS[3]['netIncome']) *100
        all_measures= pd.DataFrame.from_dict(FinMetrics,orient='index')
        all_measures = all_measures.T
        return all_measures


    revenue = [IS[0]['revenue'],IS[1]['revenue'],IS[2]['revenue'],IS[3]['revenue']]
    revenue = np.array(revenue).sum()
    net_income = [IS[0]['netIncome'],IS[1]['netIncome'],IS[2]['netIncome'],IS[3]['netIncome']]
    net_income = np.array(net_income).sum()

    FCF = CF[0]['freeCashFlow'] + CF[1]['freeCashFlow'] + CF[2]['freeCashFlow'] + CF[3]['freeCashFlow']
    OperatingCF = CF[0]['operatingCashFlow'] + CF[1]['operatingCashFlow'] + CF[2]['operatingCashFlow'] + CF[3]['operatingCashFlow']
    total_debt = BS[0]['totalDebt']

    eps_diluted = [IS[0]['epsdiluted'],IS[1]['epsdiluted'],IS[2]['epsdiluted'],IS[3]['epsdiluted']]
    eps_diluted = np.array(eps_diluted).sum()

    total_shareholders_equity = BS[0]['totalStockholdersEquity']
    shareholders_equity_2_quarters_Average = (BS[0]['totalStockholdersEquity'] + BS[1]['totalStockholdersEquity']) / 2

    total_assets_2qts = (BS[0]['totalAssets'] + BS[1]['totalAssets']) / 2

    latest_Annual_Dividend = companydata[0]['lastDiv']
    price = companydata[0]['price']
    market_Capitalization = companydata[0]['mktCap']
    name = companydata[0]['companyName']
    # exchange = companydata[0]['exchange']
    EBITDATTM = IS[0]['ebitda'] + IS[1]['ebitda'] + IS[2]['ebitda'] + IS[3]['ebitda']
    EBITDA5YTTM = IS[20]['ebitda'] + IS[21]['ebitda'] + IS[22]['ebitda'] + IS[23]['ebitda']
    EBITDA5YGrowht = (( EBITDATTM - EBITDA5YTTM) / EBITDA5YTTM)*100 # add to fin metrics
    revenue_ytd = IS[0]['revenue']+ IS[1]['revenue']+IS[2]['revenue']+IS[3]['revenue']
    revenue_10yrs = IS[36]['revenue']+ IS[37]['revenue']+IS[38]['revenue']+IS[39]['revenue']
    reveune_growth  = ((revenue_ytd - revenue_10yrs)/ revenue_10yrs) * 100


    grossprofitma12TTMgrowht = ((IS[0]['grossProfitRatio'] - IS[4]['grossProfitRatio']) / IS[4]['grossProfitRatio']) *100 # add to fin metrics. Seems flawed

    dividend_Yield= latest_Annual_Dividend/price
    FinMetrics[company] = {}
    FinMetrics[company]['Revenue growth 10 yrs'] = reveune_growth
    # FinMetrics[company]['Dividend_Yield'] = dividend_Yield * 100
    FinMetrics[company]['latest price'] = price
    # FinMetrics[company]['latest_Dividend'] = latest_Annual_Dividend
    # FinMetrics[company]['market_Capit_in_M'] = market_Capitalization/1000000
    FinMetrics[company]['P/E (diluted)'] = price / eps_diluted
    FinMetrics[company]['P/S'] = market_Capitalization / revenue
    FinMetrics[company]['P/BV'] = market_Capitalization / (total_shareholders_equity - BS[0]['goodwillAndIntangibleAssets'])
    # FinMetrics[company]['PEG'] = FinMetrics[company]['pe'] / EBITDA5YGrowht
    FinMetrics[company]['Gross profit ratio'] = IS[0]['grossProfitRatio']
    FinMetrics[company]['latest Financials date'] = IS[0]['date']
    FinMetrics[company]['Gross profit ratio growth 1 yr'] = grossprofitma12TTMgrowht
    # FinMetrics[company]['Revenue of last6qts in millions'] = [IS[0]['revenue'],IS[1]['revenue'],IS[2]['revenue'],IS[3]['revenue'],IS[4]['revenue'],IS[5]['revenue']]
    # FinMetrics[company]['Revenue of last6qts in millions'] = np.array(FinMetrics[company]['Revenue_last6qts_inM']) / 1000000

    FinMetrics[company]['P/CFO'] = market_Capitalization / OperatingCF

    FinMetrics[company]['P/FCF'] = market_Capitalization / FCF
    FinMetrics[company]["Current Ratio"] = BS[0]['totalCurrentAssets']/BS[0]['totalCurrentLiabilities']
    FinMetrics[company]["Quick Ratio"] = BS[0]['cashAndShortTermInvestments']/BS[0]['totalCurrentLiabilities']
    FinMetrics[company]['Debt to Equity'] = total_debt / total_shareholders_equity
    FinMetrics[company]['ROE'] = net_income / shareholders_equity_2_quarters_Average
    FinMetrics[company]['ROA'] = net_income / total_assets_2qts
    if IS[0]['interestExpense'] != 0:
        FinMetrics[company]['Interest Coverage'] = (IS[0]['ebitda'] - IS[0]['depreciationAndAmortization'])/IS[0]['interestExpense']

    FinMetrics[company]['revenue growth from last year'] = ((IS[0]['revenue'] -  IS[4]['revenue'])/ IS[3]['revenue']) *100
    FinMetrics[company]['earnings growth from last year'] = ((IS[0]['netIncome'] -  IS[4]['netIncome'])/ IS[3]['netIncome']) *100
    all_measures= pd.DataFrame.from_dict(FinMetrics,orient='index')
    all_measures = all_measures.T
    return all_measures


@app.route('/fundamentals/<ticker>')
def get_fundamentals(ticker):
    df = get_table(ticker)
    # df = pd.DataFrame([[1, 2], [1, 3], [4, 6]], columns=['A', 'B'])
    #return render_template('view.html',tables=[females.to_html(classes='female')]
    #return df.to_html(header="true", table_id="table")
    return render_template('view.html',tables=[ df.to_html(header="true", table_id="table")],
    titles = df.columns.values)

@app.route('/candlesticks/<ticker>')
def plot_candle(ticker):
    fig = create_candle(ticker)
    plotly.offline.plot(fig, filename='src/templates/hi.html')
    return render_template('hi.html')

    # fig.show()
    # plt.savefig('/static/images/new_plot.png')
    # return render_template('view.html', name = 'new_plot', url ='/static/images/new_plot.png')
    # output = io.BytesIO()
    # FigureCanvas(fig).print_png(output)
    # return Response(output.getvalue(), mimetype='new_plot1/png')

def create_candle(ticker):
    df = pdr.get_data_yahoo(ticker,  '2019-09-04', today)
    fig = go.Figure(data=[go.Candlestick(x=df['2019':'2020'].index.get_level_values(0),
                open=df['Open'],
                high=df['High'],
                low=df['Low'],
                close=df['Close'])])

    return fig

# # @app1.routes('/<ticker>')
# # def get_candles(ticker):

# #     fig = create_candle(ticker)
# #     app1.layout = html.Div([
# #     dcc.Graph(figure=fig)
# #     ])
# #     return app1.layout
# @app1.callback(dash.dependencies.Output('page-content', 'children'),
#               [dash.dependencies.Input('url', 'ticker')])
# def display_page(ticker):
#     fig = create_candle(ticker)
#     app1.layout = html.Div([
#     dcc.Graph(figure=fig)
#     ])
#     return app1.layout


@app.route('/options/calls/<ticker>')
def get_calls_table(ticker):
    df = get_calls(ticker)
    # df = pd.DataFrame([[1, 2], [1, 3], [4, 6]], columns=['A', 'B'])
    #return render_template('view.html',tables=[females.to_html(classes='female')]
    #return df.to_html(header="true", table_id="table")
    return render_template('index_calls.html',tables=[ df.to_html( table_id="table")],
    titles = df.columns.values)

def get_calls(ticker):
    chain = options.get_options_chain(ticker)
    return chain['calls']


@app.route('/options/puts/<ticker>')
def get_puts_table(ticker):
    df = get_puts(ticker)
    # df = pd.DataFrame([[1, 2], [1, 3], [4, 6]], columns=['A', 'B'])
    #return render_template('view.html',tables=[females.to_html(classes='female')]
    #return df.to_html(header="true", table_id="table")
    return render_template('index_puts.html',tables=[ df.to_html(header="true", table_id="table")],
    titles = df.columns.values)

def get_puts(ticker):
    chain = options.get_options_chain(ticker)
    return chain['puts']



if __name__ == "__main__":
    app.run(port=5000,debug=True)

    # app1.run_server(port=5001, debug=True, use_reloader=False)
# from flask import Flask
# import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# import pandas_datareader as pdr
# from matplotlib.legend import Legend
# from yahoo_fin import stock_info as si
# import pandas as pd
# #You need to use following line [app Flask(__name__]
# app = Flask(__name__)
# @app.route('/')
# def index():
#     return "Hello World with flask"
# if __name__ == '__main__':
#     app.run(port=5000,debug=True)
